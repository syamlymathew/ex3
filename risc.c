#include<unistd.h>
#include<stdio.h>
#include<math.h>
FILE *fop;
int allRun=0;
int range_func(int[],int);
int iqr_func(int[],int,int);
float sd_func(int[],int);
float cv_func(int[],int);
void main(char argc, char **argv)
{
	FILE *fp;
	int i=0,j=0,a[100],n=0,temp,c,total,range,iqr;
	float sd,cv;
	fp = fopen(argv[1],"r");
	fop = fopen("output.dat","a+");
	while(fscanf(fp,"%d",&a[i]) > 0)
	{

	 i++;
	n++;
	}


	for(i=0;i<n;i++)
	{
		for(j=i+1;j<n;j++)
		{
			if(a[i]>a[j])
			{
				temp=a[i];
				a[i]=a[j];
				a[j]=temp;
			}
		}
	}
if (strcmp(argv[2],"6") == 0) 
{
	allRun=1;
	c = 6;
}

else
	allRun=0;
do {
	if (strcmp(argv[2],"6") !=0)
	{
		printf("\n\t1.Range \n\t2.Interquartile range \n\t3.Standard Deviation \n\t4.Coefficient of Variation\n\t5.Exit\n\tEnter your choice:");
		scanf("%d",&c);
	}
	switch(c)
	{
		 case 6 : 
			 range=range_func(a,n);
			 //printf("\t\tRange = %d",range);
			 fprintf(fop,"Range = %d \n",range); 
			 iqr=iqr_func(a,n,total);
			 //printf("\t\tInterquartile range = %d",iqr);
			 fprintf(fop,"IQR = %d \n",iqr);
			 sd=sd_func(a,n);
			 //printf("\t\tStandard Deviation = %f",sd);
			 fprintf(fop,"SD = %f \n",sd);
			 cv=cv_func(a,n);
			 //printf("\t\tCoefficient of variation = %f",cv);
			 fprintf(fop,"CV = %f \n",cv);
			 exit(0);
		case 1 : range=range_func(a,n);
			 printf("\t\tRange = %d",range);
			 fprintf(fop,"Range = %d \n",range);
			 break;
		case 2 : iqr=iqr_func(a,n,total);
			 printf("\t\tInterquartile range = %d",iqr);
			 fprintf(fop,"IQR = %d \n",iqr);
			 break;
		case 3 : sd=sd_func(a,n);
			 printf("\t\tStandard Deviation = %f",sd);
			 fprintf(fop,"SD = %f \n",sd);
			 break;
		case 4 : cv=cv_func(a,n);
			 printf("\t\tCoefficient of variation = %f",cv);
			 fprintf(fop,"CV = %f \n",cv);
			 break;
		 case 5 : 
		 	exit(0);
		default :printf("\n Wrong Choice");
	}

} while (1==1);
 printf("\nResult has been written to the output file...\nPress any key to exit...\n");
}
int range_func(int a[],int n){
	int range=a[n-1]-a[0];
//			 printf("\n Range:%d\t\n ",range);
	return range;
}
int iqr_func(int a[], int n, int total)
{
	if(n%2==0){
		total = n;
	}
	else{
		total = n+1;
	}
	int lower = (.25)*total;
//	printf("\n Total: %d\t",lower);
	int upper = (.75)*total;
	float lower_value = (a[lower]+a[lower+1])/2;
	float upper_value = (a[upper]+a[upper+1])/2;
	float total_value = upper_value-lower_value;
//	printf("\n IQR: %f\t\n",total_value);
return total_value;
}

float sd_func(int a[], int n){
	float avg,ch,var;
	float sd,b[50];
	int i;
	for(i=0;i<n;i++)
				avg=avg+a[i];
			 avg=avg/n;
			 for(i=0;i<n;i++)
			 {
				ch=a[i]-avg;
				ch=ch*ch;
				b[i]=ch;
			 }
			 for(i=0;i<n;i++)
				var=var+b[i];
			 var=var/n;
			 sd=sqrt(var);
			return sd;
			 
}
float cv_func(int a[], int n)
{
	int i;
	float avg;
	float sd,cv;
	for(i=0;i<n;i++)
		avg=avg+a[i];
	avg=avg/n;
	sd= sd_func(a,n);
	cv=sd/avg;
//	printf("\n Coefficient of relation: %f\t\n",cv);
	return cv;
}
