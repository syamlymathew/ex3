#!/bin/sh

echo "Main program is running"
cat /dev/null > output.dat
echo '\n' "Please enter the input filename : " 
read ipFilename

while true; do

echo '\n' "Please choose any of the below option " '\n' "1 for Mean Median Mode " '\n' "2 for Range, InterQuarter Range and Standard Deviation " '\n' "3 for Graphs in libreoffice "'\n' "4 for exit "
read readOption

if  ( [ "$readOption" = "1" ] || [ "$readOption" = "2" ] )
then
	echo -n "Valid entries "
fi

case "$readOption" in
  1)
    echo "Calling mmm"
    ./mmm $ipFilename 0
    ;;
  2)
    echo "Calling risc"
    ./risc $ipFilename 0
    ;;
  3) 
    cat /dev/null > output.dat
    ./mmm $ipFilename 5
    ./risc $ipFilename 6
   echo "Calling libreoffice "
   libreoffice --calc /home/mtech/Desktop/rm1/CTGRAPH.ods
    ;;
  4)
    echo '\n' "Good Bye "
    break
    ;;
  *)
	echo '\n' "Invalid entries, please try again "
    ;;
esac

#if ( [ "$readOption" <> "4" ] && [ "$readOption" <> "1" ] && [ "$readOption" <> "2" ] )
#then
#	echo '\n' "Invalid entries, please try again "
#fi
done

